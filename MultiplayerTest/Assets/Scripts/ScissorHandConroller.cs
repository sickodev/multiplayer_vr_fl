using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ScissorHandConroller : MonoBehaviour
{

    [SerializeField] InputActionReference triggerInputAction;

    Animator scissorAnimator;
    int TriggerHash;
    Collider other;

    void Awake()
    {
        scissorAnimator = GetComponent<Animator>();

        TriggerHash = Animator.StringToHash("Trigger");

    }

    private void OnEnable()
    {
        triggerInputAction.action.performed += TriggerPressed;
    }

    private void TriggerPressed(InputAction.CallbackContext obj)
    {
        float trigger = obj.ReadValue<float>();
        trigger = (float)Math.Round(trigger, 2);
        scissorAnimator.SetFloat(TriggerHash,trigger);        
    }



    private void OnDisable()
    {
        triggerInputAction.action.performed -= TriggerPressed;
    }
}
