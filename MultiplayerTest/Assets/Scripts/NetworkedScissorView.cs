using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkedScissorView : MonoBehaviour
{
    public GameObject surfaceLower;
    public GameObject surfaceUpper;

    public void OnSelect()
    {
        surfaceLower.GetComponent<MeshRenderer>().enabled = false;
        surfaceUpper.GetComponent<MeshRenderer>().enabled = false;

    }

    public void OnDeselect()
    {

        surfaceLower.GetComponent<MeshRenderer>().enabled = true;
        surfaceUpper.GetComponent<MeshRenderer>().enabled = true;

    }
}
