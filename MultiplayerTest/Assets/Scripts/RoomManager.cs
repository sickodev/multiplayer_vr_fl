using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class RoomManager : MonoBehaviourPunCallbacks
{

    public TextMeshProUGUI  occupants;
    
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

        if(PhotonNetwork.IsConnectedAndReady)
        {
            PhotonNetwork.JoinLobby();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region UI Callback Methods

    public void JoinRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public void OnEnterButtonClicked()
    {
        Debug.Log("Joined Operation Room");
        PhotonNetwork.JoinRandomRoom();
    }

    #endregion

    #region Photon Callback Methods

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log(message);
        CreateAndJoinRoom();
    }

    public override void OnCreatedRoom()
    {
        Debug.LogWarning("Room created with name : "+ PhotonNetwork.CurrentRoom.Name);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Local Player Entered in " + PhotonNetwork.CurrentRoom.Name );
        Debug.LogWarning("Name : " + PhotonNetwork.NickName + "\nTotal Players : "+PhotonNetwork.CurrentRoom.PlayerCount );

        //Load Operation Room
        PhotonNetwork.LoadLevel("TestScene");
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("Joined The Lobby");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
       Debug.Log("Remote Player Entered in " + PhotonNetwork.CurrentRoom.Name);
       Debug.LogWarning("Name : "+newPlayer.NickName + "\nTotal Players : "+PhotonNetwork.CurrentRoom.PlayerCount);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        if(roomList.Count == 0)
        {
            //There is no room.
            occupants.text = 0 + "/ " + 20;
        }

        foreach(RoomInfo room in roomList)
        {
            Debug.Log(room.Name);
            occupants.text = room.PlayerCount + " / " + 20;
        }
    }

    #endregion

    #region Private Methods

    private void CreateAndJoinRoom()
    {
        string roomName = "Room " + Random.Range(0,100);
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 10;

        PhotonNetwork.CreateRoom(roomName,roomOptions);
    }

    #endregion
}
