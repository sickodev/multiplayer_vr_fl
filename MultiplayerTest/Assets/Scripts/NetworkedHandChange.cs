using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NetworkedHandChange : MonoBehaviour
{
    private GameObject customHand_Scissor;
    private GameObject customHand_Hand;

    public GameObject CustomVRPlayer;

    public void OnScissorSelected()
    {
        customHand_Scissor = PhotonNetwork.Instantiate("ScissorHand", customHand_Hand.transform.position, customHand_Hand.transform.rotation);
        customHand_Scissor.transform.parent = CustomVRPlayer.transform.Find("Avatar/RightHand");
        PhotonNetwork.Destroy(customHand_Hand);
    }

    public void OnScissorDeselected()
    {
        customHand_Hand = PhotonNetwork.Instantiate("CustomHandRight", customHand_Scissor.transform.position, customHand_Scissor.transform.rotation);
        customHand_Hand.transform.parent = CustomVRPlayer.transform.Find("Avatar/RightHand");
        PhotonNetwork.Destroy(customHand_Scissor);
    }
}
