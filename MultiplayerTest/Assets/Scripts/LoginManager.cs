using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class LoginManager : MonoBehaviourPunCallbacks
{

    public TMP_InputField InputFieldPlayerName;
    
    #region  Unity Methods
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #endregion

    #region UI Callbacks

    public void ConnectAnoymously()
    {
        PhotonNetwork.NickName = "Guest "+Random.Range(1,10);
        PhotonNetwork.ConnectUsingSettings();
    }

    public void ConnectToPhotonServers()
    {
        if(InputFieldPlayerName != null)
        {
            PhotonNetwork.NickName = InputFieldPlayerName.text;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    #endregion

    #region  Photon Methods

    public override void OnConnected()
    {
        Debug.Log("Server is Available");
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected To Master Server.\n" + "Name : "+PhotonNetwork.NickName);
        PhotonNetwork.LoadLevel("HomeScene");
    }

    #endregion
}
