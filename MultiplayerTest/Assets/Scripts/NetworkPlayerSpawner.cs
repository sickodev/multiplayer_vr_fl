using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NetworkPlayerSpawner : MonoBehaviourPunCallbacks
{
    private GameObject spawnedPlayerPrefab;
    public Vector3 spawnPosition;

    #region Photon Callbacks

    public override void OnJoinedRoom()
    {
        spawnedPlayerPrefab = PhotonNetwork.Instantiate("Custom Avatar",spawnPosition,Quaternion.identity);
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.Destroy(spawnedPlayerPrefab);
    }

    #endregion
}
