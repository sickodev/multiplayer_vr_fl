using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class VirtualWorldManager : MonoBehaviourPunCallbacks
{
    #region Photon Callbacks

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("A New Player Has Entered The Room");
        Debug.LogWarning("Name : "+ newPlayer.NickName +"\nTotal Players : " + PhotonNetwork.CurrentRoom.PlayerCount);
    }

    #endregion
}
